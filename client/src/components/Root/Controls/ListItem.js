import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';

import { confirmAlert } from 'react-confirm-alert';
import 'react-confirm-alert/src/react-confirm-alert.css';
import ModalVideo from 'react-modal-video';
import '../../../../node_modules/react-modal-video/scss/modal-video.scss';

import Image from './Image';
import '../../styles.css';



class ListItem extends Component {
    constructor () {
        super()
        this.state = {
          isOpen: false
        }
        this.openModal = this.openModal.bind(this)
    }

    openModal () {
        this.setState({isOpen: true})
      }

    presentConfirm = (movie) => {
        confirmAlert({
          title: 'Warning!',
          message: 'Are you sure you want to delete "' + movie.name + '"?',
          buttons: [
            {
              label: 'Yes',
              onClick: () => this.deleteMovie(movie.id)
            },
            {
              label: 'No'
            }
          ]
        });
      };
    

    deleteMovie(movieId) {
        axios.delete('api/v1/movies/' + movieId)
        .then((res)=> {
            
            if (res.data.deleted === true) {
                this.props.getMovies(true, {msg: 'Movie deleted. Sad to see it go :(', type:'success'});
            }
            else {
                this.props.getMovies(false, {msg: 'Something went wrong. Movie not deleted.', type:'error'});
            }
        })   
    }

    render() {
        let { id, name, rating, genre, explicit, imdb, image, trailerYTid} = this.props.movie;
        let movie = this.props.movie;
        let getMovies = this.props.getMovies

        const starImage = "https://i.ibb.co/dWp7zhL/JD-13-512.png";
        
        
        return (
            <li className="list-group-item center">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-3 col-md-4 col-sm-6 col-xs-12"><Image url={image} height="209" width="140"/></div>
                        <div className="col-lg-9 col-md-8 col-sm-6 col-xs-12 center">
                            <div className="row"><h5><b>{name}</b></h5></div>
                            <div className="row text-secondary">{genre}</div>
                            <div className="row text-secondary">{explicit ? 'Explicit' : 'Non explicit'}</div>
                            <div className="row text-secondary text-decoration-none">
                                <a href={imdb} className="text-secondary">
                                    IMDb: <Image url={starImage} height="30" width="30"/>
                                    <span className="text-secondary">{rating}</span>
                                </a>
                            </div>
                            <div className="row position-bottom">
                                <div className="col-lg-4 col-sm-12 pl-0 pr-0">
                                    <ModalVideo channel='youtube' isOpen={this.state.isOpen} autoplay="1" videoId={trailerYTid} onClose={() => this.setState({isOpen: false})} />
                                    <button type="button" className="btn btn-warning btn-block text-white" onClick={this.openModal}> Watch trailer</button>
                                </div>
                                <div className="col-lg-2 col-sm-6 pl-0 pr-0  offset-lg-4 ">
                                    <Link className="text-decoration-none" to={{pathname: '/edit=' + id, getMovies: getMovies}}><button type="button" className="btn btn-secondary btn-block">Edit</button></Link>
                                </div>
                                <div className="col-lg-2 col-sm-6 pl-0 pr-0">
                                    <button type="button" className="btn btn-danger btn-block" onClick={() => this.presentConfirm(movie)}>Delete</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </li>
        );
    };
}

export default ListItem;