import React from 'react';

const Image = ({url, height, width}) => {
    return (
        <img src={url} height={height} width={width} alt=""/>
    );
};

export default Image;