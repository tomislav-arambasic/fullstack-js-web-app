import React from 'react';
import { BrowserRouter, Route, Redirect } from 'react-router-dom';

import Navbar from './Navbar';
import MovieEditor from '../Forms/MovieEditor';
import Body from '../Root/Body';

function App() {
  return (
      <div>
        
        <div>
          <Navbar/>
          <BrowserRouter>
            <div className="container">
                <Route render={() => <Redirect to={{pathname: "/"}} />} />
                <Route path="/edit=:id" component={MovieEditor}/>
                <Route path="/add" component={MovieEditor}/>
                <Route path="/" exact component={Body}/>     
            </div>
        </BrowserRouter>
        </div>
      </div>
  );
}

export default App;
