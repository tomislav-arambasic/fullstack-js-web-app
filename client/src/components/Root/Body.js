import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import _ from 'lodash';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.min.css';

import List from './Controls/List';
import ListItem from './Controls/ListItem';

class Body extends Component {
    constructor(props) {
        super(props);

        this.state = ({
            loading: true,
            movies: [],
        });

        this.getMovies = this.getMovies.bind(this)
    }
    

    presentToast = (msg, toastType) => toast(msg, { type: toastType });

    getMovies(refresh, toast) {
        if (refresh) {
            axios.get('api/v1/movies')
            .then((res)=> {
                this.setState({
                    loading:false,
                    movies: _.orderBy(res.data, ['id'], ['desc'])
                })
            })
        }
        if (toast) {
            this.presentToast(toast.msg, toast.type)
        }
    }

    render() {
         if (this.state.loading === true) {
            this.getMovies(true);
            return( <h3>Loading...</h3>);
        }

        return (
            <div className="container">
                <div className="row text-right margin-bottom">
                    <div className="col-12">
                    <Link className="text-decoration-none" to={{pathname: '/add', getMovies: this.getMovies}} ><button type="add" className="btn btn-success">Add movie</button></Link>
                    </div>
                </div>
                    <List>
                        <ul className="list-group">
                            {this.state.movies.map(movie => (<ListItem key={movie.id} movie={movie} getMovies={this.getMovies}/>))}
                        </ul>
                    </List>
                    <ToastContainer toastClassName="toastStyle" autoClose={8000}/>
            </div>
        );
    }
};

export default Body;