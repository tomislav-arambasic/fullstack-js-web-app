import React from 'react';
import  '../styles.css'

const Navbar = () => {
    return (
        <nav className="navbar navbar-dark sticky-top bg-dark mb-4 justify-content-center">
            <span className="navbar-brand">Definitely not IMDb</span>
        </nav>
    );
};

export default Navbar;