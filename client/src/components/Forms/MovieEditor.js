import React, { Component } from 'react';
import InsertEditForm from '../Forms/InsertEditForm';
import axios from 'axios';

class MovieEditor extends Component{
    state = {
        loading: true,
        movie: {}
    };

    componentDidMount() {
        var id = null;
        if (this.props.match.params.id == null) {
            this.setState({
                loading: false,
                movie: {
                    id: null,
                    name: '',
                    genre: '',
                    rating: 5,
                    explicit: false,
                    imdb: '',
                    image: ''
                }
            })
        }
        else {
            id = this.props.match.params.id;
            axios.get(`api/v1/movies/${id}`).then((res) => {
                this.setState({
                    loading: false,
                    movie: res.data
                })
            });
        }
    }

    render() {
        if (this.state.loading)
        {
            return (
                <h3>Loading...</h3>
            )
        }
        return (
            <div>
                <InsertEditForm movie={this.state.movie} getMovies={this.props.location.getMovies}/>
            </div>
        )
    }
};

export default MovieEditor;