import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import axios from 'axios';
import _ from 'lodash';
import StarRatings from 'react-star-ratings';
// import { ToastContainer, toast } from 'react-toastify';
// import 'react-toastify/dist/ReactToastify.min.css';

import SelectOption from './Controls/SelectOption';
import Input from './Controls/Input';
import Select from './Controls/Select';
import SubmitCancelButtons from './Controls/SubmitCancelButtons';
import '../styles.css';


const expression = /[-a-zA-Z0-9@:%._+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_+.~#?&//=]*)/;
const urlRegex = RegExp(expression)
const formValid = ({ formErrors, ...rest }) => {
    let valid = true;

    // validate form errors being empty
    Object.values(formErrors).forEach(val => {
      val.length > 0 && (valid = false);
    });
  

    delete rest.id; // remove id validation 'coz of shared form (insert)
    // validate the form was filled out
    Object.values(rest).forEach(val => {
      val === null && (valid = false);
    });
  
    return valid;
  };

class InsertEditForm extends Component {
    constructor(props) {
        super(props);

        this.state = {
            redirectToRoot: false,
            id: props.movie.id,
            name: props.movie.name,
            genre: props.movie.genre,
            rating: props.movie.rating,
            explicit: props.movie.explicit,
            image: props.movie.image,
            imdb: props.movie.imdb,
            formErrors: {
                name: '',
                genre: '',
                rating: '',
                explicit: '',
                image: '',
                imdb: ''
            }
        };
    }

    // presentToast = (msg, toastType) => toast(msg, { type: toastType });

    onSubmit = e => {
        e.preventDefault();

        if (formValid(this.state)) {
            console.log(`
              --SUBMITTING--
                Id: ${this.state.id}
                Name: ${this.state.name}
                Genre: ${this.state.genre}
                Rating: ${this.state.rating}
                Explicit: ${this.state.explicit}
                imdb: ${this.state.imdb}
                image: ${this.state.image}
            `);

            const movieObj = {
                name: this.state.name,
                genre: this.state.genre,
                rating: this.state.rating,
                explicit: this.state.explicit === 'true' || this.state.explicit === true ? true : false,
                image: this.state.image === '' ? 'https://cdn.samsung.com/etc/designs/smg/global/imgs/support/cont/NO_IMG_600x600.png' : this.state.image,
                imdb: this.state.imdb
            } 

            if (this.state.id != null) {
                movieObj.id = this.state.id;
                axios.put('api/v1/movies/' + this.state.id, movieObj)
                .then(() => {
                    this.setState({
                        redirectToRoot: true,
                        toast: {
                            msg: 'Movie updated successfully.',
                            type: 'success'
                        }
                    })
                })
                .catch((err) => {
                    // this.presentToast('Something went wrong! ' + err, 'error');
                });
            }
            else {
                axios.post('api/v1/movies/', movieObj)
                .then(() => {
                    this.setState({
                        redirectToRoot: true,
                        toast: {
                            msg: 'Movie added successfully.',
                            type: 'success'
                        }
                    })
                })
                .catch((err) => {
                    // this.presentToast('Something went wrong! ' + err, 'error');
                });
            }
        } 
        else {
            console.error("FORM INVALID - DISPLAY ERROR MESSAGE");
        }
    };

    handleChange = e => {
        e.preventDefault();
        const { name, value } = e.target;
        let formErrors = { ...this.state.formErrors };

        switch (name) {
            case "name":
                formErrors.name = 
                value.replace(/\s+/g, '').length < 3  ? "Minimum 3 characters required" : '';
                break;
            case "genre":
                    formErrors.genre = 
                    value.replace(/\s+/g, '').length < 3 ? "Minimum 3 characters required" : '';
                    break;
            case "imdb":
                formErrors.imdb =
                urlRegex.test(value.replace(/\s+/g, '')) || value.length === 0 ? '' : "Invalid URL";
                break;
            case "image":
                formErrors.image =
                urlRegex.test(value.replace(/\s+/g, '')) || value.length === 0 ? '' : "Invalid URL";
                break;
            default:
                break;
        }
        this.setState({ formErrors, [name]: value });
    }

    buttonDisabled() {
        let formErrors = { ...this.state.formErrors };

        return formErrors.name === '' && formErrors.genre === '' && formErrors.imdb === '' && formErrors.image === ''
        && this.state.name !== '' && this.state.genre !== '';
    }

    renderRatingOptions() {
        var optionsArray = [];
        for (var i=1; i < 10; i++)
            optionsArray.push(i);

        return _.map(optionsArray, rating => {
            return (<SelectOption key={rating} value={rating} text={rating}/>)
        });
    }

    changeRating(rating) {
        this.setState({
            rating: rating
        })
      }

    render() {
        const { name, genre, rating, explicit, image, imdb } = this.state;
        const { formErrors } = this.state;

        if (this.state.redirectToRoot===true) {
            this.props.getMovies(true, this.state.toast);
            return <Redirect to="/" />
        }

        return (
            <form onSubmit={this.onSubmit} noValidate>
                <Input htmlFor="name" required={true} labelText="Title" formErrors= {formErrors} type="text" name="name" value={name} onChange={this.handleChange} errorProp={formErrors.name}/>
                <Input htmlFor="genre" required={true} labelText="Genre" formErrors= {formErrors} type="text" name="genre" value={genre} onChange={this.handleChange}  errorProp={formErrors.genre} />
                {/* <Select htmlFor="rating" labelText="Rating" name="rating" id="rating"  value={rating} onChange={this.handleChange}>
                    {this.renderRatingOptions()}
                </Select> */}
                <Select htmlFor="explicit" labelText="Explicit" name="explicit" id="explicit"  value={explicit} onChange={this.handleChange}>
                    <option value="true">Yes</option>
                    <option value="false">No</option>
                </Select>
                <Input htmlFor="image" required={false} labelText="Image URL" formErrors= {formErrors} type="text" name="image" value={image} onChange={this.handleChange}  errorProp={formErrors.image}/>
                <Input htmlFor="imdb" required={false} labelText="Imdb URL" formErrors= {formErrors} type="text" name="imdb" value={imdb} onChange={this.handleChange}  errorProp={formErrors.imdb}/>
                <p className="text-danger">* - required fields</p>
                <p htmlFor="rating">Rating</p>
                <StarRatings
                    rating={rating}
                    starRatedColor="orange"
                    changeRating={(rating) => this.changeRating(rating)}
                    numberOfStars={10}
                    name='rating'
                />
                <SubmitCancelButtons submitDisabled={!this.buttonDisabled()} cancelLinksTo='./'/>
                {/* <ToastContainer toastClassName="toastStyle" autoClose={8000}/> */}
            </form>
        )
    }
}

export default InsertEditForm;