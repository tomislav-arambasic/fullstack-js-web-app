import React from 'react';

const Input = ({htmlFor, labelText, required, type, name, value, onChange, errorProp}) => {


    // not working ? <span className="text-danger" required={true}>*</span> logs fine results
    // console.log(required)
    // return (
    //     <div className="form-group">
    //         <label htmlFor={htmlFor}>{labelText}</label> <span className="text-danger" required={true}>*</span>
    //         <input
    //             className={errorProp.length > 0 ? "error form-control" : "form-control"} 
    //             type= {type}
    //             name= {name}
    //             value={value}
    //             noValidate
    //             onChange={onChange}
    //         />
    //         {errorProp.length > 0 && (
    //             <span className="errorMessage">{errorProp}</span>
    //         )}
    //     </div>
    // ) 
    if (required) {
        return (
            <div className="form-group">
                <label htmlFor={htmlFor}>{labelText}</label> <span className="text-danger">*</span>
                <input
                    className={errorProp.length > 0 ? "error form-control" : "form-control"} 
                    type= {type}
                    name= {name}
                    value={value}
                    noValidate
                    onChange={onChange}
                />
                {errorProp.length > 0 && (
                    <span className="errorMessage">{errorProp}</span>
                )}
            </div>
        )
    }
    else {
        return (
            <div className="form-group">
                <label htmlFor={htmlFor}>{labelText}</label>
                <input
                    className={errorProp.length > 0 ? "error form-control" : "form-control"} 
                    type= {type}
                    name= {name}
                    value={value}
                    noValidate
                    onChange={onChange}
                />
                {errorProp.length > 0 && (
                    <span className="errorMessage">{errorProp}</span>
                )}
            </div>
        )
    }
};

export default Input;