import React from 'react';


const SelectOption = ({value, text}) => {
        return (
            <option value={value}>{text}</option>
        )}

export default SelectOption;