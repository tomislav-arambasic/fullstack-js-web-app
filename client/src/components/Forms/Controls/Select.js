import React from 'react';

const Select = ({children, htmlFor, labelText, type, name, id, value, onChange}) => {
    return (
        <div className="form-group">
            <label htmlFor={htmlFor}>{labelText}</label>
            <select name={name} type={type} className="form-control" id={id} value={value} onChange={onChange}>
                {children}
            </select>
        </div>
    )
};

export default Select;