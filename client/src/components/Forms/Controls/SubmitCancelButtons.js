import React from 'react';
import { Link } from 'react-router-dom';

const SubmitCancelButtons = ({submitDisabled, cancelLinksTo}) => {
    return (
        <div className="d-flex justify-content-between align-items-center margin-top-30">
            <button type="submit" className="btn btn-success" disabled={submitDisabled}>Submit</button>
            <Link to={cancelLinksTo} className="text-decoration-none"><button type="button" className="btn btn-danger">Cancel</button></Link>
        </div>
    )
};

export default SubmitCancelButtons;