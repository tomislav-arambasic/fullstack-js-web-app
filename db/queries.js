const db = require('./connection');

module.exports = {
    getAll() {
        return db.select().from('movies');
    },

    getMovie(movieId) {
        return db.first().from('movies').where({id: movieId})
    },

    insert(movieObj) {
        return db('movies').insert(movieObj).returning('*')
    },

    update(movieId, movieObj) {
        return db('movies').where({id: movieId}).update({
                name: movieObj.name,
                rating: movieObj.rating,
                genre: movieObj.genre,
                explicit: movieObj.explicit,
                imdb: movieObj.imdb,
                image: movieObj.image
            })
            .returning('*')
    },

    delete(movieId) {
        return new Promise((resolve) => {
            db.first().from('movies').where({id: movieId})
            .then((movie)=> {
                if (movie) {
                    db('movies').where({id: movieId}).del()
                    .then(()=> {
                        resolve({
                            deleted: true
                        });
                    })
                }
                else
                    resolve({
                        "error": {
                            "message": "Non existing movie ID"
                        }
                    });
            })
        })
    },

    uniqueName(movieName) {
        return new Promise((resolve) => {
            db('movies').select().first().where({name: movieName})
            .then((res) => {
                resolve(res === undefined);
            })
        });
    }
}