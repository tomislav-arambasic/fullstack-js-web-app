exports.up = (knex, Promise) => {
    return knex.schema.createTable('movies', (table) => {
        table.increments();
        table.string('name', 50).notNullable().unique();
        table.string('genre', 50).notNullable();
        table.integer('rating').notNullable();
        table.boolean('explicit').notNullable();
        table.string('image');
        table.string('imdb');
        table.string('trailerYTid', 50);
    });
};
exports.down = (knex, Promise) => {
    return knex.schema.dropTable('movies');
};