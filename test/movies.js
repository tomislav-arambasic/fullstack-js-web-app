const movies = [
    {
        "id": 1,
        "name": "The Land Before Time",
        "genre": "Fantasy",
        "rating": 7,
        "explicit": false,
        "image": "https://m.media-amazon.com/images/M/MV5BNDVhZjVmZWYtYTE0OC00MGFjLWI1YWQtZmJhNmE5NzI4ZWE4XkEyXkFqcGdeQXVyMzczMzE2ODM@._V1_UX182_CR0,0,182,268_AL_.jpg",
        "imdb": "https://www.imdb.com/title/tt0095489/?ref_=fn_al_tt_1"
    },
    {
        "id": 2,
        "name": "Jurassic Park",
        "genre": "Science Fiction",
        "rating": 9,
        "explicit": true,
        "image": "https://m.media-amazon.com/images/M/MV5BMjM2MDgxMDg0Nl5BMl5BanBnXkFtZTgwNTM2OTM5NDE@._V1_UX182_CR0,0,182,268_AL_.jpg",
        "imdb": "https://www.imdb.com/title/tt0107290/?ref_=fn_al_tt_1"
    },
    {
        "id": 3,
        "name": "Ice Age: Dawn of the Dinosaurs",
        "genre": "Animation | Adventure | Comedy ",
        "rating": 5,
        "explicit": false,
        "image": "https://m.media-amazon.com/images/M/MV5BMTMzNDkzMTcyOV5BMl5BanBnXkFtZTcwNDIzMjM2MQ@@._V1_UY268_CR1,0,182,268_AL_.jpg",
        "imdb": "https://www.imdb.com/title/tt1080016/?ref_=fn_al_tt_1"
    },
    {
        "id": 4,
        "name": "Stuber",
        "genre": " Action | Comedy",
        "rating": 6,
        "explicit": true,
        "image": "https://m.media-amazon.com/images/M/MV5BOGE1ZjFhYzAtYWM4ZC00NGI1LWFmYzMtZWRhZDhjMjE4YzBjXkEyXkFqcGdeQXVyODQzNTE3ODc@._V1_UY209_CR0,0,140,209_AL_.jpg",
        "imdb": "https://www.imdb.com/title/tt7734218/?ref_=inth_ov_i"
    },
    {
        "id": 5,
        "name": "Crawl",
        "genre": "Action | Adventure | Horror | Thriller",
        "rating": 7,
        "explicit": true,
        "image": "https://m.media-amazon.com/images/M/MV5BNjQxMzYyMDItZmUyNy00MGE0LWIwYmItMTMxYmZlOGZlMTlhXkEyXkFqcGdeQXVyNjg2NjQwMDQ@._V1_UY209_CR0,0,140,209_AL_.jpg",
        "imdb": "https://www.imdb.com/title/tt8364368/?ref_=inth_ov_i"
    },
    {
        "id": 6,
        "name": "The farewell",
        "genre": " Comedy | Drama",
        "rating": 9,
        "explicit": false,
        "image": "https://m.media-amazon.com/images/M/MV5BMWE3MjViNWUtY2VjYS00ZDBjLTllMzYtN2FkY2QwYmRiMDhjXkEyXkFqcGdeQXVyODQzNTE3ODc@._V1_UY209_CR0,0,140,209_AL_.jpg",
        "imdb": "https://www.imdb.com/title/tt8637428/?ref_=inth_ov_i"
    },
    {
        "id": 7,
        "name": "The Art of Self-Defense",
        "genre": "Comedy ",
        "rating": 9,
        "explicit": false,
        "image": "https://m.media-amazon.com/images/M/MV5BZDlkOGE4YTUtYWRlZS00YjFkLWE3NmUtNzNlNjdiZTk2NzdhXkEyXkFqcGdeQXVyNDY2MjcyOTQ@._V1_UY209_CR0,0,140,209_AL_.jpg",
        "imdb": "https://www.imdb.com/title/tt7339248/?ref_=inth_ov_i"
    },
    {
        "id": 8,
        "name": "Super 30",
        "genre": "Biography ",
        "rating": 8,
        "explicit": false,
        "image": "https://m.media-amazon.com/images/M/MV5BM2ZjZmQzMzctZjQ0Yy00ZTcxLWI1MDEtZWU4NjRiMTEyNTNmXkEyXkFqcGdeQXVyNzkxOTEyMjI@._V1_UY209_CR8,0,140,209_AL_.jpg",
        "imdb": "https://www.imdb.com/title/tt7485048/?ref_=inth_ov_i"
    }
]

const movie = {
    "name": "Annabelle Comes Home",
    "genre": "Horror | Mystery | Thriller",
    "rating": 5,
    "explicit": true,
    "image": "https://m.media-amazon.com/images/M/MV5BYmI4NDNiMmQtZTFkYi00ZDVmLThlYTAtMWJlMjU1M2I2ZmViXkEyXkFqcGdeQXVyNjg2NjQwMDQ@._V1_UY209_CR0,0,140,209_AL_.jpg",
    "imdb": "https://www.imdb.com/title/tt8350360/?ref_=inth_ov_i"
  };
  
  module.exports = {
    movies,
    movie
  }