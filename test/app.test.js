const request = require('supertest');
const expect = require('chai').expect;
const knex = require('../db/connection');

const moviesArray = require('./movies');
const app = require('../app');


describe('Movies API', () => {
    before((done) => {
      knex.migrate.rollback()
      .then(()=> {
        knex.migrate.latest()
        .then(() => {
          return knex.seed.run();
        }).then(() => done());
      })
    });
  
    it('Gets all movies', (done) => {
      request(app)
        .get('/api/v1/movies')
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(200)
        .then((response) => {
          expect(response.body).to.be.a('array');
          expect(response.body).to.deep.equal(moviesArray.movies);
          done();
        });
    });
  
    it('Gets movie by id', (done) => {
      request(app)
        .get('/api/v1/movies/1')
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(200)
        .then((response) => {
          expect(response.body).to.be.a('object');
          expect(response.body).to.deep.equal(moviesArray.movies[0]);
          done();
        });
    });
  
    it('Gets movie by id', (done) => {
      request(app)
        .get('/api/v1/movies/5')
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(200)
        .then((response) => {
          expect(response.body).to.be.a('object');
          expect(response.body).to.deep.equal(moviesArray.movies[4]);
          done();
        });
    });
  
    it('Adds new movie', (done) => {
      request(app)
        .post('/api/v1/movies')
        .send(moviesArray.movie)
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(200)
        .then((response) => {
          expect(response.body).to.be.a('object');
          moviesArray.movie.id = response.body.id;
          expect(response.body).to.deep.equal(moviesArray.movie);
          done();
        });
    });
  
    it('Updates movie', (done) => {
      moviesArray.movie.rating = 5;
      request(app)
        .put('/api/v1/movies/9')
        .send(moviesArray.movie)
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(200)
        .then((response) => {
          expect(response.body).to.be.a('object');
          expect(response.body).to.deep.equal(moviesArray.movie);
          done();
        });
    });
  
    it('Deletes movie', (done) => {
      request(app)
        .delete('/api/v1/movies/9')
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(200)
        .then((response) => {
          expect(response.body).to.be.a('object');
          expect(response.body).to.deep.equal({
            deleted: true
          });
          done();
        });
    });
  });
  
