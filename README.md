----------------------------------------------------------------------------------------
iOLAP_TASK - MOVIES DB, API, SPA, CRUD
----------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------
Project setup
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------

1) Clone the project.

Clone repository using git and command prompt or Git GUI of your choice:
https://bitbucket.org/tomislav-arambasic/iolap-api-crud-spa/src/master/
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
2) Install project node package modules (server and client).

Using terminal in project directory run the following commands:
cd client
npm install
cd ../
npm install
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
3) Create databases.

Log into psql via terminal using your psql username and credentials (psql -U 'yourUsername' + password after).
After logging, run following queries:
CREATE DATABASE iolap_db;
CREATE DATABASE iolap_db_test;

Or open pgAdmin and run those 2 queries using pgAdmin query tool.
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
4) Set up connection info.

Open knexfile.js and change 'username' and 'password' properties of 'development' and 'test' objects / environments
to match your PSQL username and password. 
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
5) Run it!

Execute 'npm test' command to test API.
Execute 'npm start' command to run server and web App.
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------

Happy CRUD-ing!




-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
TODO list:
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------

v1.0
(x) - code and project conventions, best practices
(x) - responsive design
(x) - destructure ListItem component
(x) - destructure InsertEditForm component
(x) - fix toast - pass the parameter to the Body component, change it's state and make it call toast instead of ListItem component calling toast

v1.1
(x) - add video modal and add trailer links to db
(x) - add star ratings instead of select component
() - toast on insert + edit fix
---> (x) fix insert <---
---> (x) handle duplicate unique key insert <---
---> () fix double sourcing root page bug <---
---> (x) trim start and end spaces in insert/edit form <---
() - add drop image file instead of URLs for images
() - add search in nav