const express = require ('express');
const router = express.Router();
const queries = require('../db/queries')


checkInvalidIDinput = (input) => {
    if (isNaN(parseInt(input)))
        return false;
    else
        return true;
}

isValidId = (req, res, next) => {
    if(!isNaN(parseInt(req.params.id))) return next();
    next(new Error('Invalid ID'));
}

isValidMovie = (movie) => {
    const name = typeof movie.name == 'string' && movie.name.trim() != '' && movie.name.length <= 50;
    const genre = typeof movie.genre == 'string' && movie.genre.trim() != '' && movie.genre.length <= 50;
    const explicit = (movie.explicit == true || movie.explicit == false) || (movie.explicit == 'true' || movie.explicit == 'false');
    const rating = !isNaN(parseInt(movie.rating)) && movie.rating != undefined && movie.rating != null && movie.rating > 0 && movie.rating < 11;
    return name && genre && explicit && rating;
}

router.get('/', (req, res) => {
    // res.send('GET ALL successful')
    // db.select().from('movies')
    queries.getAll()
    .then((data) => {
        res.json(data);
    });
});

router.get('/:id', isValidId, (req, res, next) => {
    // res.send('GET successful')
    queries.getMovie(req.params.id)
    .then((movie) => {
       if (movie) {
        res.json(movie);   
       }
       else {
           res.status(404);
           next(new Error('Not Found'));
       }
    })
});

router.post('/', (req, res, next) => {
    // res.send('POST successful')
    if (isValidMovie(req.body)){
        
        queries.uniqueName(req.body.name)
        .then(unique => {
            if (unique===true) {
                queries.insert(req.body)
                .then((movies) => {
                    res.json(movies[0]);
                });
            }
            else {
                res.status(409);
                next(new Error('Movie with same name alredy in database.'));
            }
        })
    }
    else {
        next(new Error('Invalid movie'));
    }
});

router.put('/:id', isValidId, (req, res, next) => {
    // res.send('PUT successful')
    if (isValidMovie(req.body)) {
        queries.update(req.params.id, req.body)
        .then((movies) => {
            res.json(movies[0]);
        });
    }
    else {
        next(new Error('Invalid movie'));
    }
  });

router.delete('/:id', isValidId, (req, res) => {
    // res.send('DELETE successful')
    queries.delete(req.params.id)
    .then((response) => {
        res.send(response)
    })
});

module.exports = router;