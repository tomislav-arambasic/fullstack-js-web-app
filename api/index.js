const express = require('express');
const router = express.Router();
const getMoviesRoute = require('./movies')
router.use('/movies', getMoviesRoute)

module.exports = router;